import { Component, OnInit } from '@angular/core';
import { Observable,fromEvent,interval,Subject, of, BehaviorSubject } from 'rxjs';
import { throttleTime, debounceTime, distinctUntilChanged, reduce, scan, pluck, mergeMap, switchMap } from 'rxjs/operators'
import { map,filter } from "rxjs/operators";
import { spawn } from 'child_process';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // Vanilla Javscript
    var likeButton= document.getElementById('likeButton')
    likeButton.addEventListener('click',(e)=> console.log('Event Listener from Vannila JS',e))
    // RxJS 
    var shareButton= document.getElementById('shareButton')
    var observer = {
      next:function(value){
        console.log('RxJS:Value from Observer',value);
      },
      error:function(err){
        console.error(err);  
      },
      complete:function(){
        console.log('Completed')
      }
    }
    // fromEvent(shareButton,'click')
    // .subscribe(observer)
    const syncEvents = Observable.create((obs)=>{
      obs.next('A value 1');
      obs.next('A value 2');
      setTimeout(()=> { 
        obs.next('A value 3');
        obs.complete(); 
      },2000)// this marks the observer as complete as its done
      // obs.error("Error");// this marks the observer as complete as its done
      
    }).subscribe(observer)
    const buttonSubscription = Observable.create((obs)=>{
      shareButton.onclick = (event)=>{
        obs.next(event.clientX);
      }
    }).subscribe(observer)

    const logObservable = interval(1000);
    const logObserver = {
      next:function(value){
        console.log(value);        
      }
    }
    logObservable.pipe(
      map(function(value) {
      return value;
     }),
     throttleTime(2000)
     )//.subscribe(logObserver)

     var subject = new Subject();
     subject.subscribe(observer)


    setTimeout(()=> {
      syncEvents.unsubscribe();
      buttonSubscription.unsubscribe();
    },5000)

    const filterObservable = interval(1000);
    filterObservable.pipe(filter(function(value){
      return value % 2 == 0;
    })).subscribe(observer)
    
    const textFeild = document.getElementById('textFeild')
    const textFeildObservable = fromEvent(textFeild, 'input');
    textFeildObservable.pipe(map(event => event.target.value),debounceTime(1000),distinctUntilChanged())
      .subscribe(observer)

    const ofObservable = of(1,2,3,4,5)
    ofObservable.pipe(reduce((total,currentValue) => total + currentValue,0)).subscribe({
      next:function(value){
        console.log('of Observable',value)
      }
    })
    const scanObservable = of(1,2,3,4,5)
    scanObservable.pipe(scan((total,currentValue) => total + currentValue,0)).subscribe({
      next:function(value){
        console.log('scan Observable',value)
      }
    })

    const pluckTextFeild = document.getElementById('pluckTextFeild')
    const pluckTextFeildObservable = fromEvent(pluckTextFeild, 'input');
    pluckTextFeildObservable.pipe(pluck('target','value'),debounceTime(1000),distinctUntilChanged())
      .subscribe(observer)

    const firstName = document.getElementById('firstName');
    const lastName = document.getElementById('lastName');
    const span = document.getElementById('combinedText');
    const combineObservable1 = fromEvent(firstName,'input')
    const combineObservable2 = fromEvent(lastName,'input')
    combineObservable1.pipe(mergeMap(event1 => {
      return combineObservable2.pipe(map(event2 => event1.target.value + " " + event2.target.value))
    })).subscribe(combinedValue => span.textContent = combinedValue)

    const switchButton = document.getElementById('switchButton');
    const switchObservable = fromEvent(switchButton, 'click');
    const intervalObservable = interval(1000);

    switchObservable.pipe(switchMap(
      event => {
        return intervalObservable
      }
    )).subscribe((value) => console.log('RxJS>Switch Map Value: ',value))
    
    const buttonObserver = {
      next:function(value){
        console.log(value)
      }
    }
    const subjectButton = document.getElementById('behaviourSubject')
    const logAction = document.getElementById('action');
    const clickEmitted = new BehaviorSubject('Not Clicked!')
    subjectButton.addEventListener('click',()=> clickEmitted.next('Clicked!'));
    clickEmitted.subscribe(
      (value) => logAction.textContent = value
    );   
  }

  

}
